# dist

Public Project used to store "@systra/name" package registries

To authorize projects to publish to the package registry through CI/CD, add them to the list of CI/CD Job token permissions of dist project
https://gitlab.com/systra/dev4bim/lib/dist/-/settings/ci_cd
